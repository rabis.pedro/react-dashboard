#!/usr/bin/env sh
platform='unknown'
unamestr=$(uname)
if [ $unamestr = 'Linux' ]; then
	platform='Linux'
elif [ $unamestr = 'Darwin' ]; then
	platform='Darwin'
fi

# DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DIR="$( pwd )"

# [ -d "$DIR/teste" ] && echo " ### Não Instale."

if [ $platform = 'Linux' ]; then
	echo "*** Sistema identificado: LINUX🐧️ ***"
	echo "Diretório Raíz do Projeto: $DIR"

	# BACKEND ---------------------------------------------------------------
	if [ -d "$DIR/back/node_modules" ]; then
		echo "\n"
		echo ">>> Dependências do Backend já estão instaladas <<<"
		echo "Iniciando o Backend... ... ..."
		gnome-terminal -- bash -c 'cd "$DIR" && cd ./back; yarn start; $SHELL'
	else
		echo "\n"
		echo ">>> Sem dependências <<<"
		echo "Instalando dependências do Backend... ... ..."
		gnome-terminal -- bash -c 'cd "$DIR" && cd ./back; yarn; yarn start; $SHELL'
	fi

	# FRONTEND --------------------------------------------------------------
	if [ -d "$DIR/front/node_modules" ]; then
		echo "\n"
		echo ">>> Dependências do Frontend já estão instaladas <<<"
		echo "Iniciando o Frontend... ... ..."

		gnome-terminal -- bash -c 'cd "$DIR" && cd ./front; yarn start; $SHELL'
	else
		echo "\n"
		echo ">>> Sem dependências <<<"
		echo "Instalando dependências do Frontend... ... ..."

		gnome-terminal -- bash -c 'cd "$DIR" && cd ./front; yarn; yarn start; $SHELL'
	fi

elif [ $platform = 'Darwin' ]; then
	echo "\n"
	echo "*** Sistema Identificado: MacOS 💻️ ***"
	echo "Diretório Raíz do Projeto: $DIR"

  # -------
  if [ -d "$DIR/back/node_modules" ]; then
		echo "\n"
		echo ">>> Dependências do Backend já estão instaladas <<<"
		echo "Iniciando o Backend... ... ..."
    osascript -e 'tell application "Terminal" to do script "cd '$DIR'/back && yarn start"'
	else
		echo "\n"
		echo ">>> Sem dependências <<<"
		echo "Instalando dependências do Backend... ... ..."

	  osascript -e 'tell application "Terminal" to do script "cd '$DIR'/back && yarn && yarn start"'
	fi

  if [ -d "$DIR/front/node_modules" ]; then
		echo "\n"
		echo ">>> Dependências do Frontend já estão instaladas <<<"
		echo "Iniciando o Frontend... ... ..."
    osascript -e 'tell application "Terminal" to do script "cd '$DIR'/front && yarn start"'
	else
		echo "\n"
		echo ">>> Sem dependências <<<"
		echo "Instalando dependências do Frontend... ... ..."

	  osascript -e 'tell application "Terminal" to do script "cd '$DIR'/front && yarn && yarn start"'
	fi
  # ------
fi
