import 'react-toastify/dist/ReactToastify.css';
import { createGlobalStyle } from 'styled-components';

// Global styles
export const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  html {
    font-size: 62.5%;

    body, #root {
      min-height: 100vh;
      background: #f5f5f5;
      color: #403f5b;
    }

    body {
      font-family: 'Montserrat', 'Roboto', 'Segoe UI','Oxygen',
      'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
      sans-serif, -apple-system, BlinkMacSystemFont;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
    }

    a {
      text-decoration: none;
    }

    li {
      list-style: none;
    }

    button {
      cursor: pointer;
      border: none;
    }

    input, textarea {
      font-family: 'Montserrat', 'Roboto', 'Segoe UI','Oxygen';
    }

    code {
      font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
        monospace;
    }

    body .Toastify .Toastify__toast-container .Toastify__toast--success {
      background: ${({ theme }) => theme.colors.primary};
      color: ${({ theme }) => theme.colors.text};
      font-size: ${({ theme }) => theme.font.sizes.xsmall};

      & .Toastify__progress-bar--success {
        background-color: ${({ theme }) => theme.colors.success};
      }
      svg {
        fill: ${({ theme }) => theme.colors.success};
      }
    }

    body .Toastify .Toastify__toast-container .Toastify__toast--warning {
      background: ${({ theme }) => theme.colors.primary};

      color: ${({ theme }) => theme.colors.text};
      font-size: ${({ theme }) => theme.font.sizes.xsmall};

      & .Toastify__progress-bar--warning {
        background-color: ${({ theme }) => theme.colors.warning};
      }
      svg {
        fill: ${({ theme }) => theme.colors.warning};
      }
    }

    body .Toastify .Toastify__toast-container .Toastify__toast--error {
      font-size: ${({ theme }) => theme.font.sizes.xsmall};
    }


  }
`;
