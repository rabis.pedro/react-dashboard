import { AxiosResponse } from 'axios';
import React, {
  createContext,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useState,
} from 'react';
import { IUsers, User } from '../domain/Users';
import { userService } from '../services/users';

type InputDataUser = {
  id?: number;
  name: string;
  email: string;
};

type UserContextType = {
  users: User[];
  handleSetUsers: React.Dispatch<React.SetStateAction<IUsers | null>>;
  getUsers: () => void;
  inputForm: InputDataUser;
  setInputForm: React.Dispatch<React.SetStateAction<InputDataUser>>;
  isChange: boolean;
  setIsChange: React.Dispatch<React.SetStateAction<boolean>>;
  isChecked: boolean;
  handleChecked: (e: boolean) => void;
};

type UserContextProviderProps = {
  children: ReactNode;
};

export const UserContext = createContext({} as UserContextType);

const UsersProvider = ({ children }: UserContextProviderProps) => {
  const [users, setUsers] = useState([]);
  const [inputForm, setInputForm] = useState({ name: '', email: '' });
  const [isChange, setIsChange] = useState(false);
  const [isChecked, setIsChecked] = useState(false);

  function handleChecked(e: boolean) {
    setIsChecked(e);
  }

  const handleSetUsers = useCallback((data) => {
    setUsers(data);
  }, []);

  const getUsers = async () => {
    const response: AxiosResponse<IUsers> = await userService.getAllUsers('/users');
    handleSetUsers(response.data);
    setIsChange(false);
  };

  useEffect(() => {
    getUsers();
  }, [isChange]);

  return (
    <UserContext.Provider
      value={{
        users,
        handleSetUsers,
        getUsers,
        inputForm,
        setInputForm,
        isChange,
        setIsChange,
        isChecked,
        handleChecked,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

function useMyUsers() {
  const context = useContext(UserContext);

  if (!context) {
    throw new Error('useUtils must be used within an UserProvider');
  }

  return context;
}

export { UsersProvider, useMyUsers };
