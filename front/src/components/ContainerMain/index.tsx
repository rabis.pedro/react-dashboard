import React from 'react';
import { WrapperContainerMain, WrapperContentMain } from './styles';
import { ContainerMainProps, ContentProps } from './types';

export function ContainerMain({ children }: ContainerMainProps) {
  return <WrapperContainerMain>{children.map((child) => child)}</WrapperContainerMain>;
}

export function ContentMain({ children }: ContentProps) {
  return <WrapperContentMain>{children}</WrapperContentMain>;
}
