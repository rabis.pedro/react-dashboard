import styled from 'styled-components';

export const WrapperSwitchButton = styled.div<{ isChecked: boolean }>`
  display: flex;
  align-items: center;
  justify-content: center;

  width: 7rem;
  height: 3.3rem;

  & > label {
    display: flex;
    align-items: center;
    position: relative;

    cursor: pointer;
    height: 100%;
    width: 100%;
    border-radius: ${({ theme }) => theme.borderRadius.regular};
    padding: ${({ theme }) => theme.spacing.extraTight};
    background: ${({ theme }) => theme.colors.gray};
    box-shadow: inset ${({ theme }) => theme.shadow.shadow2};
    overflow: hidden;

    input {
      visibility: visible;
      display: none;
    }

    & > svg {
      width: 3rem;
      height: 3rem;
      transition: ${({ theme }) => theme.transition.default};
      transform: translateX(${({ isChecked }) => (isChecked ? '0' : '-2.5rem')});
      position: absolute;
      padding: 0.2rem;
      border-radius: ${({ theme }) => theme.borderRadius.regular};
      transition: ${({ theme }) => theme.transition.default};

      &:nth-of-type(1) {
        display: block;
        transform: translateX(${({ isChecked }) => (isChecked ? '-2.5rem' : '0')});
        fill: ${({ theme }) => theme.colors.background};
        left: 0.3rem;
        background: ${({ theme }) => theme.colors.primary};
        opacity: ${({ isChecked }) => (isChecked ? '0' : '1')};
        transform: translate(${({ isChecked }) => (isChecked ? '2.5rem' : '0')});
      }

      &:nth-of-type(2) {
        display: block;
        transform: translateX(${({ isChecked }) => (isChecked ? '0' : '2.5rem')});
        fill: ${({ theme }) => theme.colors.secondary};
        right: 0.1rem;
        background: ${({ theme }) => theme.colors.background};
        opacity: ${({ isChecked }) => (isChecked ? '1' : '0')};
        transform: translate(${({ isChecked }) => (isChecked ? '0' : '-2.5rem')});
      }
    }
  }
`;
