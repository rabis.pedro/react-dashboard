import styled from 'styled-components';

export const WrapperHeader = styled.header`
  display: flex;
  /* flex-direction: column; */
  /* grid-area: Header; */
  justify-content: space-between;

  padding: 1.5rem 1.5rem;
  width: 100%;
  overflow: hidden;
  white-space: nowrap;
  box-shadow: ${({ theme }) => theme.shadow.shadow1};
  background-color: ${({ theme }) => theme.colors.white};

  @media (max-width: 576px) {
    width: 100vw;
  }

  & > span:first-child {
    & > div:first-child {
      display: flex;
      align-items: center;
      svg {
        margin-right: 1rem;
      }

      h1 {
        font-size: 3rem;
        font-weight: 500;
      }
    }

    & > div:last-child {
      h2 {
        font-weight: ${({ theme }) => theme.font.weights.light};
        font-size: ${({ theme }) => theme.font.sizes.small};
        margin-top: ${({ theme }) => theme.spacing.extraTight};
      }
    }
  }

  & > span:last-child {
    display: flex;
    align-items: center;
    justify-content: flex-end;
  }
`;
