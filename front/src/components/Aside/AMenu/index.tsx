import React from 'react';
import { MenuList } from './MenuList';
import { WrapperAMenu } from './styles';

export function AMenu(): JSX.Element {
  return (
    <WrapperAMenu>
      <MenuList />
    </WrapperAMenu>
  );
}
