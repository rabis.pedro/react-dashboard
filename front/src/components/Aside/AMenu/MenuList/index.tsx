import React from 'react';
import { FaHome, FaUsers } from 'react-icons/fa';
import { MenuItem } from './MenuItem';

export function MenuList(): JSX.Element {
  return (
    <ul>
      <MenuItem Icon={FaHome} title="Início" path="/" />
      <MenuItem Icon={FaUsers} title="Usuários" path="/users" />
    </ul>
  );
}
