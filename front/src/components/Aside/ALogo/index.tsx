import React from 'react';
import { Link } from 'react-router-dom';
import { ReactComponent as Logo } from '../../../assets/imgs/logo.svg';
import { WrapperALogo } from './styles';

export function ALogo(): JSX.Element {
  return (
    <WrapperALogo>
      <Link to="/">
        <Logo />
      </Link>
    </WrapperALogo>
  );
}
