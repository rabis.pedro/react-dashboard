import styled from 'styled-components';

export const WrapperALogo = styled.aside`
  display: flex;
  grid-area: ALogo;

  align-items: center;
  justify-content: center;
  border-bottom: solid 0.1rem ${({ theme }) => theme.colors.background};
  background-color: ${({ theme }) => theme.colors.primary};

  & > a {
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 0;

    svg {
      width: 170px;
    }
  }

  @media (max-width: 576px) {
    width: 100vw;

    & > a {
      svg {
        display: flex;
        width: 200px;
      }
    }
  }
`;
