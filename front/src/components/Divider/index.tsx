import React from 'react';
import { WrapperDivider } from './styles';
import { DividerProps } from './types';

export function Divider({ myHeight }: DividerProps) {
  return <WrapperDivider myHeight={myHeight ? myHeight : 0.1} />;
}
