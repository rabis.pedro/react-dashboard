import React, { useContext } from 'react';
import { FaPencilAlt, FaTrash } from 'react-icons/fa';
import { ThemeContext } from 'styled-components';
import { useMyUsers } from '../../contexts/userContext';
import { User } from '../../domain/Users';
import { userService } from '../../services/users';
import { WrapperTable } from './styles';

// create table
function RowsBodyTable(): JSX.Element {
  const { users, setInputForm, setIsChange } = useMyUsers();
  const { colors } = useContext(ThemeContext);

  function handleDeleteUser(id: number) {
    userService.deleteUser(`/users/${id}`);
    setIsChange(true);
  }

  async function handleUpdateUser(id: number) {
    const response = await userService.getUserById(`/users/${id}`);
    setInputForm({ id, name: response.data.name, email: response.data.email });
  }

  return (
    <>
      {users.map((user: User) => (
        <tr key={user.id}>
          <td>{user.id}</td>
          <td>{user.name}</td>
          <td>{user.email}</td>
          <td>
            <button onClick={() => handleUpdateUser(user.id)}>
              <FaPencilAlt size={18} color={colors.success} />
            </button>
            <button onClick={() => handleDeleteUser(user.id)}>
              <FaTrash size={18} color={colors.error} />
            </button>
          </td>
        </tr>
      ))}
    </>
  );
}

export function Table(): JSX.Element {
  return (
    <WrapperTable>
      <thead>
        <tr>
          <th>ID</th>
          <th>Nome</th>
          <th>Email</th>
          <th>Ações</th>
        </tr>
      </thead>
      <tbody>
        <RowsBodyTable />
      </tbody>
    </WrapperTable>
  );
}
