import React from 'react';
import { toast } from 'react-toastify';
import { useMyUsers } from '../../contexts/userContext';
import { User } from '../../domain/Users';
import { userService } from '../../services/users';
import { WrapperForm } from './styles';

export function Form(): JSX.Element {
  const { inputForm, setInputForm, setIsChange } = useMyUsers();

  function handleChangeName(name: string): void {
    setInputForm({ ...inputForm, name });
  }

  function handleChangeEmail(email: string): void {
    setInputForm({ ...inputForm, email });
  }

  function handleCancel() {
    setInputForm({ name: '', email: '' });
  }

  async function handleEmailExists(email: string) {
    let exists = false;
    await userService.getAllUsers('/users').then((response) => {
      const user = response.data.find((user: User) => user.email === email);

      if (user) {
        exists = true;
      }
    });

    return exists;
  }

  // validate form
  function handleValidateForm(name: string, email: string): boolean {
    if (name === '') {
      toast.error('O nome não pode ser vazio');
      return false;
    }

    if (email === '') {
      toast.error('O email não pode ser vazio');
      return false;
    }

    // regex email
    const validaEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    if (!validaEmail.test(email)) {
      toast.error('O email não é válido');
      return false;
    }

    return true;
  }

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    const isValid = handleValidateForm(inputForm.name, inputForm.email);

    let user = {} as User;

    if (isValid) {
      if (inputForm.id) {
        user = { id: inputForm.id, name: inputForm.name, email: inputForm.email };
        await userService.updateUser(`/users/${inputForm.id}`, user);
        toast.warning('Usuário atualizado com sucesso!');
      } else {
        const emailExists: boolean = await handleEmailExists(inputForm.email);
        if (!emailExists) {
          const user = {
            id: null,
            name: inputForm.name,
            email: inputForm.email,
          };
          toast.success('Usuário criado com sucesso!');
          await userService.saveUser('/users', user);
        } else {
          toast.error('O email já existe');
        }
      }
    }

    setIsChange(true);
    handleCancel();
  };

  return (
    <WrapperForm onSubmit={handleSubmit} isUpdate={!!inputForm.id}>
      <fieldset>
        <legend>Formulário de Usuários</legend>
        <div>
          <p>
            <label>
              Nome:
              <input
                type="text"
                name="name"
                value={inputForm.name}
                onChange={(e) => handleChangeName(e.target.value)}
                placeholder="Digite o nome..."
              />
            </label>
          </p>

          <p>
            <label>
              Email:
              <input
                type="text"
                name="email"
                value={inputForm.email}
                onChange={(e) => handleChangeEmail(e.target.value)}
                placeholder="Digite o email..."
              />
            </label>
          </p>
        </div>
      </fieldset>

      <div>
        <button type="submit">{inputForm.id ? 'Atualizar' : 'Salvar'}</button>
        <button type="reset" onClick={handleCancel}>
          Cancelar
        </button>
      </div>
    </WrapperForm>
  );
}
