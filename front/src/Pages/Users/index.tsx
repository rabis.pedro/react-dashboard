import React from 'react';
import { FaUsers } from 'react-icons/fa';
import { ContainerMain, ContentMain } from '../../components/ContainerMain';
import { Divider } from '../../components/Divider';
import { Form } from '../../components/Form';
import { Header } from '../../components/Header';
import { Table } from '../../components/Table';

export default function Users() {
  return (
    <ContainerMain>
      <Header MyIcon={FaUsers} title="Users" subtitle="Cadastro de Usuários!" />
      <ContentMain>
        <Form />
        <Divider />
        <Table />
      </ContentMain>
    </ContainerMain>
  );
}
